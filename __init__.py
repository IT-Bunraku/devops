subdomains = {
    'cloud': dict(icon='cloud', title="Cloud"),
    'dev':   dict(icon='code',  title="Dev"),
    'ops':   dict(icon='gears', title="Ops"),
    'fleet': dict(icon='plane', title="Fleet"),
}


from django.contrib import admin

from .models import *

################################################################################

class ProgrammingAdmin(admin.ModelAdmin):
    list_display = ('alias', 'title')

admin.site.register(ProgLang, ProgrammingAdmin)

#*******************************************************************************

class RepoAdmin(admin.ModelAdmin):
    list_display = ('owner', 'alias', 'title', 'link', 'kind', 'provider')
    list_filter  = ('kind', 'is_public', 'has_wiki', 'languages__alias', 'owner__username')

admin.site.register(ScmRepo, RepoAdmin)

################################################################################

class DynoAdmin(admin.ModelAdmin):
    list_display = ('owner', 'grid', 'alias', 'title', 'descr', 'state', 'repo')
    list_filter  = ('grid', 'state', 'repo__kind', 'repo__languages__alias', 'owner__username')

admin.site.register(OpsDyno, DynoAdmin)

#*******************************************************************************

class BackendAdmin(admin.ModelAdmin):
    list_display = ('owner', 'alias', 'target', 'state', 'dyno', 'repo')
    list_filter  = ('dyno__state', 'dyno__repo__kind', 'dyno__repo__languages__alias', 'owner__username')

admin.site.register(OpsBackend, BackendAdmin)


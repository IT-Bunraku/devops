robot.on "bitbucketPushReceived", (pushEvent) ->
    push = pushEvent.push

    repository = push.repo
    repository_url = repository.url # e.g. https://bitbucket.org/marcus/project-x/
    repository_name = repository.name # e.g. Project X
    repository_slug = repository.slug # e.g. project-x

    for commit in push.commits
        author = commit.author # bitbucket username
        branch = commit.master # e.g. master
        message = commit.message # A really clear message

        for file in commit.files
            file = file.filename # e.g. somefile.py
            type = file.type # i.e. modified, added etc


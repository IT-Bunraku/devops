# -*- coding: utf-8 -*-

from octopus.shortcuts import *

#****************************************************************************************

class SoyoustartSpider(CrawlSpider):
    name = "soyoustart"
    allowed_domains = [
        'soyoustart.com',
        'www.soyoustart.com',
    ]
    
    start_urls = (
        'http://www.soyoustart.com/ma/serveurs-essential/',
        'http://www.soyoustart.com/ma/serveurs-game/',
    )
    
    rules = (
        Rule(LinkExtractor(allow=(r'ma\/offres\/(.*)\.xml',)), callback='parse_dedicated'),
    )
    
    def parse_dedicated(self, response):
        srv = PhysicalServer()
        
        srv['link']        = response.url
        srv['brand']       = 'soyoustart'
        srv['narrow']      = cleanup(response.xpath('//div[@class="wrapper"]//h1/text()'), sep=None, trim=True, inline=True)
        
        blockHead = response.xpath('//*[@class="section3 center"][2]')
        
        srv['pricing'] = {
            'ontime':  cleanup(blockHead.xpath('./p[1]/span[1]/*/text()'), sep=None, trim=True, inline=True),
            'monthly': cleanup(response.xpath('//div[@class="full"][3]//div[@class="section2"][2]/div/span[1]//text()'), sep='', trim=True, inline=True),
        }
        
        tblSpecs = response.xpath('//*[starts-with(@class, "section1")][4]//table[1]')
        
        def clean_cpu(x):
            kv = x.split(' ')
            
            if 2 <= len(kv):
                return [
                    (kv[1], kv[0])
                ]
            
            return []
        
        srv['specs_cpu']   = dict([
            (k, v)
            for x in cleanup(tblSpecs.xpath('.//tr[2]/td/text()'), sep=' ', trim=True, inline=True).split('/')
            for k,v in clean_cpu(unicode(x).strip())
        ])
        
        srv['specs_cpu'].update({
            'model':  cleanup(tblSpecs.xpath('.//tr[1]/td/text()'), trim=True, inline=True),
            'freq':   cleanup(tblSpecs.xpath('.//tr[3]/td/text()'), trim=True, inline=True),
        })
        
        srv['specs_ram']   = {
            'raw': cleanup(tblSpecs.xpath('.//tr[4]/td/text()'), trim=True, inline=True),
        }
        
        srv['specs_hdd'] = cleanup(tblSpecs.xpath('.//tr[5]/td/text()'), trim=True, inline=True).replace('\xc2\xa0', ' ').split('x')
        
        srv['specs_raid']   = ' '.join(cleanup(tblSpecs.xpath('.//tr[6]/td/text()'), trim=True, inline=True).split(' '))
        
        if len(srv['specs_hdd'])==2:
            srv['specs_hdd'] = {
                'count': int(srv['specs_hdd'][0]),
                'raw': srv['specs_hdd'][1].strip(),
            }
        
        srv['specs_net']   = {
            'bandwidth': cleanup(tblSpecs.xpath('.//tr[8]/td/text()'), trim=True, inline=True),
            'ipv4':      cleanup(tblSpecs.xpath('.//tr[10]/td/text()'), trim=True, inline=True),
            'ipv6':      cleanup(tblSpecs.xpath('.//tr[11]/td/text()'), trim=True, inline=True),
        }
        
        for key in ('ram','hdd'):
            if type(srv['specs_'+key]['raw']) is dict:
                lst = [
                    y
                    for sep in ('Go', 'To')
                    if sep in srv['specs_'+key]['raw']
                    for y in [sep]+[
                        x.strip()
                        for x in srv['specs_'+key]['raw'].split(sep)
                    ]
                ]
                
                if len(lst)==3:
                    srv['specs_'+key]['unit'], srv['specs_'+key]['size'], srv['specs_'+key]['raw'] = lst
                
                for field in ('size','count'):
                    srv['specs_'+key][field] = int(srv['specs_'+key]['raw'].get(field, '') or '1')
                
                #del srv['specs_'+key]['raw']
                
                if srv['specs_'+key]['count']==1:
                    del srv['specs_'+key]['count']
        
        for key in ('cores','threads'):
            srv['specs_cpu'][key] = int(srv['specs_cpu'][key])
        
        return srv


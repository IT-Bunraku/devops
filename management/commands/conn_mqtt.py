#!/usr/bin/env python

from gestalt.cmd.management import *

from uchikoma.devops.shortcuts import *

import paho.mqtt.client as mqtt

################################################################################

class Command(UplinkCommand):
    help = 'MQTT uplink.'

    def add_options(self, parser):
        parser.add_argument('-H', "--host", type=str, default='iot.eclipse.org',
            dest = "host",
            help = "MQTT server's host.", 
            #metavar = "FILE"
        )
        parser.add_argument('-p', "--port", type=int, default=1883,
            dest = "port",
            help = "MQTT server's port.", 
            #metavar = "FILE"
        )
        parser.add_argument('-t', "--topic", type=str, default='$SYS/#',
            dest = "topic",
            help = "MQTT server's topic.", 
            #metavar = "FILE"
        )
        parser.add_argument('-k', "--keep", type=int, default=60,
            dest = "keep",
            help = "MQTT server's Keep-Alive.", 
            #metavar = "FILE"
        )

    #***************************************************************************

    def prepare(self, *args, **options):
        self.proxy = mqtt.Client()

        self.proxy.on_connect = self.on_connect
        self.proxy.on_message = self.on_message

        self.topic = options['topic']

    #***************************************************************************

    def loop(self, *args, **options):
        self.proxy.connect(options['host'], options['port'], options['keep'])

        print "*) Scanning SPARQL endpoints :"

        self.proxy.loop_forever()

    #***************************************************************************

    def on_connect(self, client, userdata, flags, rc):
        print("Connected with result code "+str(rc))

        self.proxy.subscribe(self.topic)

    def on_message(self, client, userdata, msg):
        print(msg.topic+" "+str(msg.payload))


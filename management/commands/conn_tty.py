#!/usr/bin/env python

from gestalt.cmd.management import *

from uchikoma.devops.shortcuts import *

import serial

################################################################################

class Command(UplinkCommand):
    help = 'TTy uplink.'

    def add_options(self, parser):
        parser.add_argument('-p', "--port", type=str, default='USB0',
            dest = "port",
            help = "TTy port to use.", 
            #metavar = "FILE"
        )
        parser.add_argument('-b', "--baud", type=int, default=9600,
            dest = "baud",
            help = "TTy port speed (in baud)", 
            #metavar = "FILE"
        )

    #***************************************************************************

    def prepare(self, *args, **options):
        self.connect('mongodb://mist:optimum@ds039135.mlab.com:39135/vortex')

    def terminate(self, *args, **options):
        pass

    #***************************************************************************

    def loop(self, *args, **options):
        options['path'] = '/dev/tty' % options

        print "*) Scanning SPARQL endpoints :"

        with serial.Serial(options['path'], options['baud'], timeout=1) as cnx:
            click.echo('Initialized the serial port :')

            data = None

            while loop:
                payload = ser.readline()

                try:
                    data = json.loads(payload)
                except:
                    data = None

                if data is not None:
                    print data


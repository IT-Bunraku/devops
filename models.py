from .utils import *

################################################################################

@Reactor.orm.register_model('prog_lang')
class ProgLang(WebResource):
    alias      = models.CharField(max_length=64)
    title      = models.CharField(max_length=256, blank=True)

    def __str__(self):     return str(self.title or self.alias)
    def __unicode__(self): return unicode(self.title or self.alias)

    class Meta:
            verbose_name        = "Programming language"
            verbose_name_plural = "Programming languages"

################################################################################

SCM_TYPEs = (
    ('svn', "Subversion"),

    ('hg',  "Mercurial"),

    ('git', "Git"),
    ('bzr', "Bazaar"),
)

@Reactor.orm.register_model('scm_repo')
class ScmRepo(WebResource):
    provider   = models.CharField(max_length=64)
    owner      = models.ForeignKey(Identity, related_name='scm_repos')
    alias      = models.CharField(max_length=64)
    kind       = models.CharField(max_length=64, default='git', choices=SCM_TYPEs)

    link       = property(lambda self: "git@%(provider)s:%(owner)s/%(alias)s.git" % self.__dict__)
    raw_data   = JSONField()

    title      = models.CharField(max_length=128, blank=True)
    summary    = models.TextField(blank=True)
    website    = models.CharField(max_length=128, blank=True)
    logo       = models.CharField(max_length=256, blank=True)

    languages  = models.ManyToManyField(ProgLang, related_name='repos', blank=True)

    is_public  = models.BooleanField(default=True, blank=True)
    has_wiki   = models.BooleanField(default=True, blank=True)

    fork_of    = models.ForeignKey('ScmRepo', related_name='forks', blank=True, null=True, default=None)

    fs_size    = models.IntegerField(default=0, blank=True)
    created_at = models.DateTimeField(blank=True, auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, auto_now_add=True)

    def __str__(self):     return str(self.link)
    def __unicode__(self): return unicode(self.link)

    @property
    def context(self):
        return {}

    #branches   = MongoProp.DictField()
    #commits    = MongoProp.DictField()
    #issues     = MongoProp.DictField()
    #events     = MongoProp.DictField()
    #followers  = MongoProp.DictField()

    class Meta:
            verbose_name        = "SCM reporitory"
            verbose_name_plural = "SCM reporitories"

#*******************************************************************************

@Reactor.orm.register_model('ops_dyno')
class OpsDyno(WebResource):
    owner = models.ForeignKey(Identity, related_name='ops_dynos')
    grid  = models.CharField(max_length=24, choices=DYNO_PLATFORMs)
    alias = models.CharField(max_length=64)

    title = models.CharField(max_length=128, blank=True)
    descr = models.TextField(blank=True)

    repo  = models.ForeignKey(ScmRepo, related_name='deploys', blank=True, null=True, default=None)
    state = models.CharField(max_length=24, choices=DYNO_STATEs)

    #raw_data   = JSONField()

    @property
    def context(self):
        return {}

    class Meta:
            verbose_name        = "PAAS dyno"
            verbose_name_plural = "PAAS dynos"

#*******************************************************************************

@Reactor.orm.register_model('ops_backend')
class OpsBackend(WebResource):
    owner    = models.ForeignKey(Identity, related_name='ops_backends')
    alias    = models.CharField(max_length=64)

    dyno     = models.ForeignKey(OpsDyno, related_name='backends', blank=True, null=True, default=None)

    target   = models.CharField(max_length=512, blank=True)

    repo     = property(lambda self: self.dyno.repo)
    link     = property(lambda self: urlparse(self.target))

    schema   = property(lambda self: self.link.scheme)
    hostname = property(lambda self: self.link.hostname)
    port     = property(lambda self: self.link.port)
    username = property(lambda self: self.link.username)
    password = property(lambda self: self.link.password)
    path     = property(lambda self: self.link.path)
    resource = property(lambda self: self.link.path[1:])

    state    = models.CharField(max_length=24, choices=DYNO_STATEs)

    #raw_data   = JSONField()

    @property
    def context(self):
        return {}

    class Meta:
            verbose_name        = "PAAS backend"
            verbose_name_plural = "PAAS backends"


#-*- coding: utf-8 -*-

from .utils import *

################################################################################

import datetime

@Reactor.mongo.register_embed('comment')
class ScmRepository(MongoDocument):
    provider   = MongoProp.StringField(max_length=64, required=True)
    owner      = MongoProp.StringField(max_length=64, required=True)
    slug       = MongoProp.StringField(max_length=128, required=True)

    title      = MongoProp.StringField(max_length=256, required=False)
    summary    = MongoProp.StringField()
    website    = MongoProp.StringField(max_length=512, required=False)
    logo       = MongoProp.StringField(max_length=512, required=False)

    branches   = MongoProp.DictField()
    commits    = MongoProp.DictField()
    issues     = MongoProp.DictField()
    events     = MongoProp.DictField()
    followers  = MongoProp.DictField()

    scm_type   = MongoProp.StringField(default='git', choices=['git', 'svn', 'hg', 'bzr'])
    language   = MongoProp.StringField(max_length=512, required=False)

    is_private = MongoProp.BooleanField(default=True)
    has_wiki   = MongoProp.BooleanField(default=False)

    fork_of    = MongoProp.DictField(required=False)
    has_forks  = MongoProp.DictField(required=False)

    curr_size  = MongoProp.IntField(default=0)
    created_at = MongoProp.DateTimeField(default=datetime.datetime.now)
    updated_at = MongoProp.DateTimeField(default=datetime.datetime.now)

    raw        = MongoProp.DictField()


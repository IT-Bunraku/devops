from django.conf.urls import include, url

from gestalt.web.helpers import Reactor

from uchikoma.devops.views import fleet as Views

################################################################################

urlpatterns = Reactor.router.urlpatterns('fleet',
    url(r'^$',                            Views.Homepage.as_view(), name='homepage'),
)


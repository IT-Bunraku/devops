from django.conf.urls import include, url

from gestalt.web.helpers import Reactor

from uchikoma.devops.views import ops as Views

################################################################################

urlpatterns = Reactor.router.urlpatterns('ops',
    url(r'^dynos/$',                      Views.dyno__list, name='dyno__list'),
    url(r'^dynos/(?P<narrow>[^/]+)/$',    Views.dyno__view, name='dyno__view'),

    url(r'^$',                            Views.Homepage.as_view(), name='homepage'),
)


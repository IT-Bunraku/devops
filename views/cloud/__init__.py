#-*- coding: utf-8 -*-

from uchikoma.devops.shortcuts import *

################################################################################

class Homepage(Reactor.ui.Dashboard):
    template_name = "cloud/views/homepage.html"

    def enrich(self, request, **context):
        context['primitives'] = enumerate_schemas(owner_id=request.user.pk)

        return context

    def populate(self, request, **context):
        for label,icon,stats in context['primitives']:
            yield Reactor.ui.Block('counter', label.lower(),
                title=label, icon=icon,
                size=dict(md=3, xs=6),
            value=stats)

################################################################################

@login_required
@render_to('rest/backend/list.html')
def backend__list(request):
    return context(
        listing=[
            d.__json__
            for d in request.user.ops_backends.all()
        ],
    )

#*******************************************************************************

@login_required
@render_to('rest/backend/view.html')
def backend__view(request, narrow):
    bkn = request.user.ops_backends.get(alias=nrw)

    return context(bkn.__json__)

################################################################################

#from . import filesystem

#from . import nestrix


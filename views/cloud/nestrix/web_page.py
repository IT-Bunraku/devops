#-*- coding: utf-8 -*-

from demantia.contrib.cloud.shortcuts import *

################################################################################

@fqdn.route(r'^nestrix/publishing/?$', strategy='login')
def listing(context):
    context.template = 'cloud/views/nestrix/web_page/list.html'

    return {'version': version}

#*******************************************************************************

@fqdn.route(r'^nestrix/publishing/(?P<provider>.+)/(?P<owner>.+)/(?P<slug>.+)$', strategy='login')
def overview(context, provider, owner, slug):
    context.template = 'cloud/views/nestrix/web_page/view.html'

    return {'version': version}

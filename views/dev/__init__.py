#-*- coding: utf-8 -*-

from uchikoma.devops.shortcuts import *

################################################################################

class Homepage(Reactor.ui.Dashboard):
    template_name = "dev/views/homepage.html"

    def enrich(self, request, **context):
        context['primitives'] = enumerate_schemas(owner_id=request.user.pk)

        return context

    def populate(self, request, **context):
        for label,icon,stats in context['primitives']:
            yield Reactor.ui.Block('counter', label.lower(),
                title=label, icon=icon,
                size=dict(md=3, xs=6),
            value=stats)

################################################################################

@login_required
@render_to('rest/repo/list.html')
def repo__list(request):
    return context(
        listing=[
            d.__json__
            for d in request.user.scm_repos.all()
        ],
    )

#*******************************************************************************

@login_required
@render_to('rest/repo/view.html')
def repo__view(request, owner, narrow):
    repo = request.user.scm_repos.get(alias=nrw)

    return context(repo.__json__)

################################################################################

#from . import basis

#from . import metalogy


#-*- coding: utf-8 -*-

from deming.contrib.dev.shortcuts import *

################################################################################

@fqdn.route(r'^containers$', strategy='login')
def listing(context):
    context.template = 'dev/views/container/list.html'

    return {'version': version}

#*******************************************************************************

@fqdn.route(r'^containers/(?P<provider>.+)/(?P<owner>.+)/(?P<slug>.+)$', strategy='login')
def overview(context, provider, owner, slug):
    context.template = 'dev/views/container/view.html'

    return {'version': version}


#-*- coding: utf-8 -*-

from deming.contrib.dev.shortcuts import *

################################################################################

@fqdn.route(r'^charms/?$', strategy='login')
def listing(context):
    context.template = 'dev/rest/repo/list.html'

    return {
        'listing': ScmRepository.objects(owner='it2use', slug__in=[x for x in CHARM_LIST]),
    }

#*******************************************************************************

@fqdn.route(r'^charms/(?P<name>.+)/?$', strategy='login')
def overview(context, name):
    context.template = 'dev/rest/repo/view.html'

    return {
        'repo': ScmRepository.get(owner='it2use', slug=narrow),
    }

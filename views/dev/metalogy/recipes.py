#-*- coding: utf-8 -*-

from deming.contrib.dev.shortcuts import *

################################################################################

@fqdn.route(r'^recipes/?$', strategy='login')
def listing(context):
    context.template = 'dev/rest/repo/list.html'

    return {
        'listing': ScmRepository.objects(slug='kyogen'),
    }

#*******************************************************************************

@fqdn.route(r'^recipes/(?P<name>.+)/?$', strategy='login')
def overview(context, narrow):
    context.template = 'dev/rest/repo/view.html'

    return {
        'repo': ScmRepository.get(owner=narrow, slug='kyogen'),
    }

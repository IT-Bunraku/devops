#-*- coding: utf-8 -*-

from uchikoma.devops.shortcuts import *

################################################################################

class Homepage(Reactor.ui.Dashboard):
    template_name = "ops/views/homepage.html"

    def enrich(self, request, **context):
        context['primitives'] = enumerate_schemas(owner_id=request.user.pk)

        return context

    def populate(self, request, **context):
        for label,icon,stats in context['primitives']:
            yield Reactor.ui.Block('counter', label.lower(),
                title=label, icon=icon,
                size=dict(md=3, xs=6),
            value=stats)

################################################################################

@login_required
@render_to('rest/dyno/list.html')
def dyno__list(request):
    return context(
        listing=[
            d.__json__
            for d in request.user.ops_dynos.all()
        ],
    )

#*******************************************************************************

@login_required
@render_to('rest/dyno/view.html')
def dyno__view(request, narrow):
    dyno = request.user.ops_dynos.get(alias=nrw)

    return context(dyno.__json__)

################################################################################

#from . import basis

#from . import metalogy


#-*- coding: utf-8 -*-

from kungfu.contrib.ops.shortcuts import *

################################################################################

@fqdn.route(r'^distros/?$', strategy='login')
def listing(context):
    context.template = 'ops/rest/distro/list.html'

    return {'version': version}

#*******************************************************************************

@fqdn.route(r'^distros/(?P<name>.+)/?$', strategy='login')
def overview(context, name):
    context.template = 'ops/rest/distro/view.html'

    return {
        'name': name,
    }

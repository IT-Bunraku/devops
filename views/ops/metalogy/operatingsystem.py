#-*- coding: utf-8 -*-

from kungfu.contrib.ops.shortcuts import *

################################################################################

@fqdn.route(r'^os/?$', strategy='login')
def listing(context):
    context.template = 'ops/rest/os/list.html'

    return {'version': version}

#*******************************************************************************

@fqdn.route(r'^os/(?P<name>.+)/?$', strategy='login')
def overview(context, name):
    context.template = 'ops/rest/os/view.html'

    return {
        'name': name,
    }

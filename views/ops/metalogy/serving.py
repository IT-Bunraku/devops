#-*- coding: utf-8 -*-

from kungfu.contrib.ops.shortcuts import *

################################################################################

@fqdn.route(r'^servers/?$', strategy='login')
def listing(context):
    context.template = 'ops/rest/server/list.html'

    return {'version': version}

#*******************************************************************************

@fqdn.route(r'^servers/(?P<name>.+)/?$', strategy='login')
def overview(context, name):
    context.template = 'ops/rest/server/view.html'

    return {
        'name': name,
    }
